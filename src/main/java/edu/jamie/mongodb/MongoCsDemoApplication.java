package edu.jamie.mongodb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import edu.jamie.mongodb.changestreams.ChangeStreamSample;
import it.ozimov.springboot.mail.configuration.EnableEmailTools;

@EnableAsync
@SpringBootApplication
@EnableEmailTools
public class MongoCsDemoApplication {

	  @Bean
	    public TaskExecutor taskExecutor() {
	        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	        executor.setCorePoolSize(5);
	        executor.setMaxPoolSize(10);
	        executor.setQueueCapacity(25);
	        return executor;
	    }
	
	  @Autowired
	  ChangeStreamSample csSample;
	  
	    @Bean
	    public CommandLineRunner schedulingRunner(TaskExecutor executor) {
	        return new CommandLineRunner() {
	            public void run(String... args) throws Exception {
	            	
	                executor.execute(csSample);
	            }
	        };
	    }
	  
	public static void main(String[] args) {
		SpringApplication.run(MongoCsDemoApplication.class, args);
	}
}
