package edu.jamie.mongodb.changestreams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.OperationType;

import edu.jamie.mongodb.dto.Event;
import edu.jamie.mongodb.repository.EventRepo;


@Component
@Scope("prototype")
public class ChangeStreamSample implements Runnable {

@Autowired 
EventRepo eventRepo;

	
	            public void run() {
	            System.out.println("Loading MongoDB ChangeStream Example");
	            MongoClient 	mongoClient = MongoClients.create(new ConnectionString("mongodb+srv://csdemouser:Password1!@cluster0-znzsp.mongodb.net/Demo?retryWrites=true"));
	       		 MongoCollection<Document> collection = mongoClient.getDatabase("Demo").getCollection("user");
	    		 List<Bson> pipeline = Collections.singletonList(Aggregates.match(Filters.or(
	    				    Filters.in("operationType", Arrays.asList("insert","replace")))));
	    		 
	    		 MongoCursor<ChangeStreamDocument<Document>> cursor = collection.watch(pipeline).iterator();
	    		 
	    		 while(true) {
	    			 ChangeStreamDocument<Document> next = cursor.next();
	    			 System.out.println("New document found: " + next.getFullDocument().toJson());
	    			 processEvent( next); 
	    			 
	    		 }
	            }
	  
	 
	 
	           private void processEvent(ChangeStreamDocument<Document> doc) {
	        	 Event newEvent = new Event();
	        	 newEvent.setName(doc.getOperationType().toString());
	        	 newEvent.setJson(doc.getFullDocument().toJson());
	        	 eventRepo.save(newEvent);
	        	 
	        	 
	           }
	            	
}
	 
	 