package edu.jamie.mongodb.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.jamie.mongodb.dto.User;
import edu.jamie.mongodb.repository.UserRepo;

@RestController
public class LoginController {

	@Autowired
	UserRepo userRepo;
	
	


	
	    @RequestMapping("/login")
	    public User greeting(@RequestParam(value="username", defaultValue="jlpetru") String username) {
	    	User user = userRepo.findByUsername(username);
	  
	    	if (user != null) {
	    		user.setLastloggedin(new Date());
	    		userRepo.save(user);
	    	
	    	}
	    	
	
	    	
	        return user;
	    
	}
}
