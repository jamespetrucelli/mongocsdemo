package edu.jamie.mongodb.dto;

public class Event {

	
	
	private String name;
	
	private String json;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
	
	
}
