package edu.jamie.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import edu.jamie.mongodb.dto.User;



public interface UserRepo extends MongoRepository<User, String>{

	public User findByUsername(String username);
	
	
}
